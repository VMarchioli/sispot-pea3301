%-------------------------------------------------------------------------%
% PEA3301 - Introducao aos Sistemas de Potencia - Resolucao P2 - 16/06/2020
% Vin�cius Marchioli - 10774232 - Prof. Pellini
% ------------------------------------------------------------------------%


%                       --------- LEIA ME ---------
% Executar os blocos separadamente para as partes da Questao 1 pressionando
% as teclas "CTRL + Enter". Pode-se tambem executar todo o script.
% Funcao auxiliar definida no final do arquivo. Pode nao funcionar em ver-
% soes antigas do MATLAB. Separe a funcao em outro arquivo de mesmo nome,
% caso necessario.
% Foram calculados apenas os parametros utilizados nos calculos.
%                       ---------------------------


% Parte I - Calculo das potencias em regime (sem defeitos). 
% Nao foram considerados os defasadores para o calculo das potencias. 
% Potencia trifasica complexa injetada na barra 6 e a perda ativa trifasica
% da linha entre as barras 3 e 9:


%------Definicao das Bases nos dominios (verificar circuito do pdf)-------%
clc;
clear;

%Potencia de Base
Sb = 100e6;

%Tensoes de Base
VbI = 13.8e3;  
VbII = 230e3;
VbIII = 14.5e3;

%Impedancias de Base
ZbII = (VbII*VbII)/Sb;
ZbIII = (VbIII*VbIII)/Sb;

%-------------------------------------------------------------------------%
%--------------------- Parametros dos equivalentes -----------------------%
%-------------------------------------------------------------------------%

%--------------------------- Equivalente 1 -------------------------------%
Vth1 = 13.8e3;
S1cc_3f = (52.8 + 528*1i)*1e6;

% Tensao em pu:
vth1 = Vth1/VbI;

% Potencias em pu:
s1cc_3f = S1cc_3f/Sb;

% Impedancia em pu:
z1_g1 = 1/conj(s1cc_3f);                    

%--------------------------- Equivalente 4 -------------------------------%
Vth4 = 14.076e3*exp(1i*(-8)*(pi/180));
S4cc_3f = (47.52 + 475.2*1i)*1e6;

% Tensao em pu:
vth4 = Vth4/VbI;

% Potencias em pu:
s4cc_3f = S4cc_3f/Sb;

% Impedancia em pu:
z1_g4 = 1/conj(s4cc_3f);                    

%--------------------------- Equivalente 6 -------------------------------%
Vth6 = 14.49e3*exp(1i*(5*pi/180));
S6cc_3f = (47.52 + 475.2*1i)*1e6;

% Tensao em pu:
vth6 = Vth6/VbI;

% Potencias em pu:
s6cc_3f = S6cc_3f/Sb;

% Impedancia em pu:
z1_g6 = 1/conj(s6cc_3f);

%-------------------------------------------------------------------------%
%------------------------ Parametros das Linhas --------------------------%
%-------------------------------------------------------------------------%

% Impedancias de sequencia positiva, em ohms/km
Zlinha_1 = (0.066 + 0.375*1i);

% Comprimentos em km
l23 = 80;
l25 = 96;
l27 = 48;
l39 = 32;
l511 = 24;

%--------------------------- Trecho 2-3 ----------------------------------%

Zl23_1 = (Zlinha_1)*l23;
zl23_1 = Zl23_1/ZbII;

%--------------------------- Trecho 2-5 ----------------------------------%

Zl25_1 = (Zlinha_1)*l25;
zl25_1 = Zl25_1/ZbII;

%--------------------------- Trecho 2-7 ----------------------------------%

Zl27_1 = (Zlinha_1)*l27;
zl27_1 = Zl27_1/ZbII;

%--------------------------- Trecho 3-9 ----------------------------------%

Zl39_1 = (Zlinha_1)*l39;
zl39_1 = Zl39_1/ZbII;

%--------------------------- Trecho 5-11 ---------------------------------%

Zl511_1 = (Zlinha_1)*l511;
zl511_1 = Zl511_1/ZbII;

%-------------------------------------------------------------------------%
%------------------------ Parametros dos Trafos --------------------------%
%-------------------------------------------------------------------------%

%------------------------------ Trafo 12 ---------------------------------%

S12 = 80e6;
zt12 = (0.01 + 0.1*1i)*(Sb/S12);

%------------------------------ Trafo 34 ---------------------------------%

S34 = 50e6;
zt34 = (0.01 + 0.1*1i)*(Sb/S34);

%------------------------------ Trafo 56 ---------------------------------%

S56 = 70e6;
zt56 = (0.01 + 0.1*1i)*(Sb/S56);

%------------------------------ Trafo 78 ---------------------------------%

S78 = 100e6;
zt78 = (0.008 + 0.08*1i)*(Sb/S78);

%----------------------------- Trafo 910 ---------------------------------%

S910 = 50e6;
zt910 = (0.008 + 0.08*1i)*(Sb/S910);

%----------------------------- Trafo 1112 --------------------------------%

S1112 = 50e6;
zt1112 = (0.008 + 0.08*1i)*(Sb/S1112);


%-------------------------------------------------------------------------%
%------------------------ Parametros das Cargas --------------------------%
%-------------------------------------------------------------------------%

%--------------------------- Carga da Barra 8 ----------------------------%

Zc8_1 = 2.19 + 0.93*1i;
zc8_1 = Zc8_1/ZbIII;

%--------------------------- Carga da Barra 10 ---------------------------%

Zc10_1 = (13.14 + 5.60*1i)/3; %triangulo para estrela
zc10_1 = Zc10_1/ZbIII;

%--------------------------- Carga da Barra 12 ---------------------------%

Zc12_1 = 3.89 + 1.66*1i;
zc12_1 = Zc12_1/ZbIII;

%-------------------------------------------------------------------------%
%------------------------ Resolucao do sistema ---------------------------%
%-------------------------------------------------------------------------%

%Define A, I e Yprimitiva:
%Resolve o sistema [A'*Y*A]*[v]=[I] para obter as tensoes nodais

Yprimitiva = diag([1/z1_g1, 1/zt12, 1/zl23_1, 1/zt34, 1/z1_g4, 1/zl25_1, 1/zt56, 1/z1_g6, 1/zl27_1, 1/zt78, 1/zc8_1, 1/zl39_1, 1/zt910, 1/zc10_1, 1/zl511_1, 1/zt1112, 1/zc12_1]);
I = [vth1/z1_g1; 0; 0; vth4/z1_g4; 0; vth6/z1_g6; 0; 0; 0; 0; 0; 0];

   %Nos: 1  2  3  4  5  6  7  8  9  10 11 12
A   =   [1  0  0  0  0  0  0  0  0  0  0  0 ; %z1_g1
         1 -1  0  0  0  0  0  0  0  0  0  0 ; %zt12
         0  1 -1  0  0  0  0  0  0  0  0  0 ; %zl23_1
         0  0  1 -1  0  0  0  0  0  0  0  0 ; %zt34
         0  0  0  1  0  0  0  0  0  0  0  0 ; %z1_g4
         0  1  0  0 -1  0  0  0  0  0  0  0 ; %zl25_1
         0  0  0  0  1 -1  0  0  0  0  0  0 ; %zt56
         0  0  0  0  0  1  0  0  0  0  0  0 ; %z1_g6
         0  1  0  0  0  0 -1  0  0  0  0  0 ; %zl27_1
         0  0  0  0  0  0  1 -1  0  0  0  0 ; %zt78
         0  0  0  0  0  0  0  1  0  0  0  0 ; %zc8_1
         0  0  1  0  0  0  0  0 -1  0  0  0 ; %zl39_1
         0  0  0  0  0  0  0  0  1 -1  0  0 ; %zt910
         0  0  0  0  0  0  0  0  0  1  0  0 ; %zc10_1
         0  0  0  0  1  0  0  0  0  0 -1  0 ; %zl511_1
         0  0  0  0  0  0  0  0  0  0  1 -1 ; %zt1112
         0  0  0  0  0  0  0  0  0  0  0  1 ];%zc12_1

e = (A.'*Yprimitiva*A)\I;

%-------------------------------------------------------------------------%
%----------------------------- Potencias ---------------------------------%
%-------------------------------------------------------------------------%

%----------------------- Pot. Injetada na barra 6 ------------------------%
s6_3f = e(6)*conj((e(6)-e(5))/zt56);  
S6_3f = s6_3f*Sb;

disp(">Potencia Injetada na Barra 6:");
txt = strcat("P = ", num2str(real(S6_3f)/1e6, '%5.4f'), ' MW');
disp(txt);
txt = strcat("Q = ", num2str(imag(S6_3f)/1e6, '%5.4f'), ' MVAr');
disp(txt);
disp(" ");

%------------------------- Perda na Linha 3 9 ----------------------------%
e39 = e(3) - e(9);
sl39 = conj(e39)*e39/zl39_1;
Sl39 = sl39*Sb;
Pl39 = real(Sl39);

disp(">Perda na Linha 3-9:");
txt = strcat("Perdas = ", num2str(Pl39/1e3, '%5.4f'), ' kW');
disp(txt);
disp(" ");

%% Parte II - Curto trifasico (ABC) entre as barras 2 e 3 a uma distancia de
% 25km da barra 2, com R = 1 ohm

% ------------------------ Matriz de Transformacao -----------------------%

alpha = 1*exp(1i*(2*pi/3));
T = [1 1 1; 
    1 alpha^2 alpha; 
    1 alpha alpha^2];

%------Definicao das Bases nos dominios (verificar circuito do pdf)-------%

%Potencia de Base
Sb = 100e6;

%Tensoes de Base
VbI = 13.8e3;  
VbII = 230e3;

%Impedancias de Base
ZbII = (VbII*VbII)/Sb;

%Corrente de Base
IbII = Sb/(sqrt(3)*VbII);

%-------------------------------------------------------------------------%
%--------------------- Parametros dos equivalentes -----------------------%
%-------------------------------------------------------------------------%
% Fontes assumiram as defasagens dos defasadores

%--------------------------- Equivalente 1 -------------------------------%
Vth1 = 13.8e3*exp(1i*(pi/180)*(30)); 
S1cc_3f = (52.8 + 528*1i)*1e6;

% Tensao em pu:
vth1 = Vth1/VbI;

% Potencias em pu:
s1cc_3f = S1cc_3f/Sb;

% Impedancia em pu:
z1_g1 = 1/conj(s1cc_3f);                   

%--------------------------- Equivalente 4 -------------------------------%
Vth4 = 14.076e3*exp(1i*(pi/180)*(22));
S4cc_3f = (47.52 + 475.2*(1i))*1e6;

% Tensao em pu:
vth4 = Vth4/VbI;

% Potencias em pu:
s4cc_3f = S4cc_3f/Sb;

% Impedancia em pu:
z1_g4 = 1/conj(s4cc_3f);                  
                      
%--------------------------- Equivalente 6 -------------------------------%
Vth6 = 14.49e3*exp(1i*(pi/180)*(35));
S6cc_3f = (47.52 + 475.2*(1i))*1e6;

% Tensao em pu:
vth6 = Vth6/VbI;

% Potencias em pu:
s6cc_3f = S6cc_3f/Sb;

% Impedancia em pu:
z1_g6 = 1/conj(s6cc_3f);                    

%-------------------------------------------------------------------------%
%------------------------ Parametros das Linhas --------------------------%
%-------------------------------------------------------------------------%

% Impedancias de sequencia positiva e negativa em ohms/km
Zlinha_1 = (0.066 + 0.375*1i);

% Comprimentos em km
l23 = 80;
l25 = 96;

%--------------------------- Trecho 2-3 ----------------------------------%

Zl23_1 = (Zlinha_1)*l23;
zl23_1 = Zl23_1/ZbII;

%--------------------------- Trecho 2-5 ----------------------------------%

Zl25_1 = (Zlinha_1)*l25;
zl25_1 = Zl25_1/ZbII;

%-------------------------------------------------------------------------%
%------------------------ Parametros dos Trafos --------------------------%
%-------------------------------------------------------------------------%

%------------------------------ Trafo 12 ---------------------------------%

S12 = 80e6;
zt12 = (0.01 + 0.1*1i)*(Sb/S12);

%------------------------------ Trafo 34 ---------------------------------%

S34 = 50e6;
zt34 = (0.01 + 0.1*1i)*(Sb/S34);

%------------------------------ Trafo 56 ---------------------------------%

S56 = 70e6;
zt56 = (0.01 + 0.1*1i)*(Sb/S56);

%-------------------------------------------------------------------------%
%------------------------ Resolucao do sistema ---------------------------%
%-------------------------------------------------------------------------%

l = 25;
R_falta = 1;
r_falta = R_falta/ZbII;
I = [vth1/z1_g1; 0; 0; vth4/z1_g4; 0; vth6/z1_g6; 0];
Yprimitiva = diag([1/z1_g1, 1/zt12, 1/zl25_1, 1/zt34, 1/z1_g4, 1/(zl23_1*((l23-l)/l23)), 1/zt56, 1/z1_g6, 1/(zl23_1*(l/l23))]);

   %Nos: 1  2  3  4  5  6  F 
A  =    [1  0  0  0  0  0  0; %z1_g1
         1 -1  0  0  0  0  0; %zt12
         0  1  0  0 -1  0  0; %zl25_1
         0  0  1 -1  0  0  0; %zt34
         0  0  0  1  0  0  0; %z1_g4
         0  0  1  0  0  0 -1; %zF3
         0  0  0  0  1 -1  0; %zt56
         0  0  0  0  0  1  0; %z1_g6
         0  1  0  0  0  0 -1];%z2F

e = (A.'*Yprimitiva*A)\I;

%-------------------------- Sequencia Positiva ---------------------------%
% As sequencias negativa e zero nao aparecem no curto trifasico (ver pdf)
vth_eq_1 = e(7);
zth_eq_1 = par((par(z1_g1 + zt12, zl25_1 + zt56 + z1_g6) + zl23_1*l/l23), (zl23_1*(l23 - l)/l23 + zt34 + z1_g4));

% Corrente
i1 = vth_eq_1/(zth_eq_1 + r_falta);

% Resolucao do Novo Sistema, com a corrente de falta no ponto F da falta
I_novo = [vth1/z1_g1; 0; 0; vth4/z1_g4; 0; vth6/z1_g6; -i1];
e_novo = (A'*Yprimitiva*A)\I_novo;

% Calculando as componentes simetricas
i0_3F = 0;
i1_3F = (e_novo(3) - e_novo(7))/(zl23_1*(l23 - l)/l23);
i2_3F = 0;

% Voltando para componentes de fase
i_3F = T*[i0_3F; i1_3F; i2_3F];
I_3F = i_3F*IbII;

%----------------------------- Resultados --------------------------------%

mod = abs(I_3F(1));
fase = rad2deg(angle(I_3F(1)));
disp(">Curto ABC");
disp(">Corrente da Fase A (da barra 3 ao defeito): ");
disp("");
txt = strcat("IA = ", num2str(mod)," /_ ", num2str(fase), '� A');
disp(txt);
disp(" ");

%% Parte III - Curto Dupla fase-terra (BCN). Mesmos parametros de antes

% ------------------------ Matriz de Transformacao -----------------------%

alpha = 1*exp(1i*(2*pi/3));
T = [1 1 1; 
    1 alpha^2 alpha; 
    1 alpha alpha^2];

%------Definicao das Bases nos dominios (verificar circuito do pdf)-------%

%Potencia de Base
Sb = 100e6;

%Tensoes de Base
VbI = 13.8e3;  
VbII = 230e3;

%Impedancias de Base
ZbI = (VbI*VbI)/Sb;
ZbII = (VbII*VbII)/Sb;

%Corrente de Base
IbI = Sb/(sqrt(3)*VbI);
IbII = Sb/(sqrt(3)*VbII);

%-------------------------------------------------------------------------%
%--------------------- Parametros dos equivalentes -----------------------%
%-------------------------------------------------------------------------%
% Fontes assumiram as defasagens dos defasadores

%--------------------------- Equivalente 1 -------------------------------%
Vth1 = 13.8e3*exp(1i*(pi/180)*(30)); 
S1cc_3f = (52.8 + 528*1i)*1e6;
S1cc_1f = (35.2 + 352*1i)*1e6;

% Tensao em pu:
vth1 = Vth1/VbI;

% Potencias em pu:
s1cc_3f = S1cc_3f/Sb;
s1cc_1f = S1cc_1f/Sb;

% Impedancia em pu:
z0_g1 = 3/conj(s1cc_1f) - 2/conj(s1cc_3f);  
z1_g1 = 1/conj(s1cc_3f);                   
z2_g1 = z1_g1;   

%--------------------------- Equivalente 4 -------------------------------%
Vth4 = 14.076e3*exp(1i*(pi/180)*(22));
S4cc_3f = (47.52 + 475.2*(1i))*1e6;
S4cc_1f = (31.68 + 316.8*(1i))*1e6;

% Tensao em pu:
vth4 = Vth4/VbI;

% Potencias em pu:
s4cc_3f = S4cc_3f/Sb;
s4cc_1f = S4cc_1f/Sb;

% Impedancia em pu:
z0_g4 = 3/conj(s4cc_1f) - 2/conj(s4cc_3f);  
z1_g4 = 1/conj(s4cc_3f);                  
z2_g4 = z1_g4;  
                      
%--------------------------- Equivalente 6 -------------------------------%
Vth6 = 14.49e3*exp(1i*(pi/180)*(35));
S6cc_3f = (47.52 + 475.2*(1i))*1e6;
S6cc_1f = (31.68 + 316.8*(1i))*1e6;

% Tensao em pu:
vth6 = Vth6/VbI;

% Potencias em pu:
s6cc_3f = S6cc_3f/Sb;
s6cc_1f = S6cc_1f/Sb;

% Impedancia em pu:
z0_g6 = 3/conj(s6cc_1f) - 2/conj(s6cc_3f);  
z1_g6 = 1/conj(s6cc_3f);                    
z2_g6 = z1_g6;

%-------------------------------------------------------------------------%
%------------------------ Parametros das Linhas --------------------------%
%-------------------------------------------------------------------------%

% Impedancias de sequencia positiva e negativa em ohms/km
Zlinha_1 = (0.066 + 0.375*1i);
Zlinha_0 = (0.483 + 1.373*1i);

% Comprimentos em km
l23 = 80;
l25 = 96;

%--------------------------- Trecho 2-3 ----------------------------------%

Zl23_1 = (Zlinha_1)*l23;
Zl23_0 = (Zlinha_0)*l23;
zl23_1 = Zl23_1/ZbII;
zl23_0 = Zl23_0/ZbII;

%--------------------------- Trecho 2-5 ----------------------------------%

Zl25_1 = (Zlinha_1)*l25;
Zl25_0 = (Zlinha_0)*l25;
zl25_1 = Zl25_1/ZbII;
zl25_0 = Zl25_0/ZbII;

%-------------------------------------------------------------------------%
%------------------------ Parametros dos Trafos --------------------------%
%-------------------------------------------------------------------------%

%------------------------------ Trafo 12 ---------------------------------%

S12 = 80e6;
zt12 = (0.01 + 0.1*1i)*(Sb/S12);
zat12_sec = 0.5*(Sb/S12);   

%------------------------------ Trafo 34 ---------------------------------%

S34 = 50e6;
zt34 = (0.01 + 0.1*1i)*(Sb/S34);

%------------------------------ Trafo 56 ---------------------------------%

S56 = 70e6;
zt56 = (0.01 + 0.1*1i)*(Sb/S56);

%-------------------------------------------------------------------------%
%------------------------ Resolucao do sistema ---------------------------%
%-------------------------------------------------------------------------%

l = 25;
R_falta = 1;
r_falta = R_falta/ZbII;

Yprimitiva = diag([1/z1_g1, 1/zt12, 1/zl25_1, 1/zt34, 1/z1_g4, 1/(zl23_1*((l23-l)/l23)), 1/zt56, 1/z1_g6, 1/(zl23_1*(l/l23))]);
I = [vth1/z1_g1;0;0;vth4/z1_g4;0;vth6/z1_g6;0];

   %Nos: 1  2  3  4  5  6  F  
A  =    [1  0  0  0  0  0  0; %z1_g1
         1 -1  0  0  0  0  0; %zt12
         0  1  0  0 -1  0  0; %zl25_1
         0  0  1 -1  0  0  0; %zt34
         0  0  0  1  0  0  0; %z1_g4
         0  0  1  0  0  0 -1; %zF3
         0  0  0  0  1 -1  0; %zt56
         0  0  0  0  0  1  0; %z1_g6
         0  1  0  0  0  0 -1];%z2F
     
e = (A.'*Yprimitiva*A)\I;

%---------------------------- Sequencia Zero -----------------------------%
zth_eq_0 = par((par(3*zat12_sec + zt12, zl25_0 + zt56) + zl23_0*l/l23), (zl23_0*(l23 - l)/l23 + zt34));

%-------------------------- Sequencia Positiva ---------------------------%
vth_eq_1 = e(7);
zth_eq_1 = par((par(z1_g1 + zt12, zl25_1 + zt56 + z1_g6) + zl23_1*l/l23), (zl23_1*(l23 - l)/l23 + zt34 + z1_g4));

%-------------------------- Sequencia Negativa ---------------------------%
zth_eq_2 = zth_eq_1;

%---------------------------- Curto Circuito -----------------------------%
% A partir de i1, usa-se um divisor de corrente e a primeira lei
i1 = vth_eq_1/(zth_eq_1 + r_falta + par(zth_eq_2 + r_falta, zth_eq_0 + r_falta));
i2 = i1*((zth_eq_0 + r_falta)/(zth_eq_2 + r_falta + zth_eq_0 + r_falta));
i0 = i1 - i2;

% Da barra 3 ate a falta na sequencia zero
i0_3F = -i0*(par(3*zat12_sec + zt12, zl25_0 + zt56) + zl23_0*l/l23)/((zl23_0*(l23 - l)/l23 + zt34) + (par(3*zat12_sec + zt12, zl25_0 + zt56) + zl23_0*l/l23));

% novo vetor de correntes com corrente injetada no ponto de falta
I_novo = [vth1/z1_g1; 0; 0; vth4/z1_g4; 0; vth6/z1_g6; -i1];
e = (A.'*Yprimitiva*A)\I_novo;

% Da barra 3 ate a falta na sequencia positiva
i1_3F = (e(3) - e(7))/(zl23_1*(l23 - l)/l23);
% Da barra 3 ate a falta na sequencia negativa
i2_3F = -i2*(par(z1_g1 + zt12, zl25_1 + zt56 + z1_g6) + zl23_1*l/l23)/(par(z1_g1 + zt12, zl25_1 + zt56 + z1_g6) + zl23_1*l/l23 + zl23_1*(l23-l)/l23 + zt34 + z1_g4);

i_3F = T*[i0_3F; i1_3F; i2_3F];
I_3F = i_3F*IbII;

%----------------------------- Resultados --------------------------------%

mod = abs(I_3F(3));
fase = rad2deg(angle(I_3F(3)));

disp(">Curto BCN");
disp(">Corrente da Fase C (da barra 3 ao defeito): ");
disp("");
txt = strcat("IC = ", num2str(mod,'%5.4f')," /_ ", num2str(fase,'%5.4f'), '� A');
disp(txt);

%% Funcao auxiliar:

% Calcula paralelo 
function ze1 = par(z1, z2)
  ze1 = 1/((1/z1)+(1/z2));
end