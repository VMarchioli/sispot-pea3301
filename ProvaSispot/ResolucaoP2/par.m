function ze1 = par(z1, z2)
  ze1 = 1/((1/z1)+(1/z2));
end