% ## Author: elpel_000 <elpel_000@LELENOVO>
% ## Created: 2020-06-08

clear;
close;
clc;

%Chama funcoes de apoio para ajudar o engenheiro
%funcs_apoio;

fprintf(1, 'Dados do problema\n');
%Depois de trazer todos os valores para a base do problema
%Gerador
zg0 = 1i*0.1*(100/20);
zg1 = zg0;
zg2 = zg1;

%Trafo
zt0 = 1i*0.03*(100/30);
zt1 = zt0;
zt2 = zt1;

%Linhas
zl0 = ((0.3+1i*0.9)*2)/((34.5*34.5)/100);
zl1 = ((0.1+1i*0.3)*2)/((34.5*34.5)/100);
zl2=zl1;

%Sistema equivalente
zseq0 = (3 / conj(1i*200/100)) - (2 / conj(1i*300/100));
zseq1 = 1 / conj(1i*300/100);
zseq2 = zseq1;


%Item A
fprintf(1, '\nItem A - corrente de defeito pra terra\n');

%equivalente da rede para seq. 0
zeq_0 = par(zt0 + zl0, zl0 + zseq0);

%equivalente da rede para seq. 1 e 2
zeq_1 = par(zg1 + zt1 + zl1, zl1 + zseq1);
zeq_2 = zeq_1;
vth_1 = FasorModFaseDeg(1, 30);
fprintf(1, 'Tensao do eq. de thevenin de seq. pos = ');
print_comp_pol(vth_1);fprintf(1, ' [pu]\n');   

%zTotal
ztotal = zeq_0 + zeq_1 + zeq_2;
i012 = vth_1 / ztotal;
fprintf(1, 'i0 = i1 = i2 = ');print_comp_pol(i012);fprintf(1, ' [pu]\n');   
IB = 100E6/(sqrt(3)*34500);
fprintf(1, 'Corrente de defeito (saindo da barra R para a terra)\n');
[iaRN, ibRN, icRN] = CSim2Phase(i012, i012, i012);
fprintf(1, 'iA = ');print_comp_pol(iaRN*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'iB = ');print_comp_pol(ibRN*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'iC = ');print_comp_pol(icRN*IB/1000);fprintf(1, ' [kA]\n');

%Item B
fprintf(1, '\nItem B - contribuicoes do sistema equivalente\n');
vr_0 = - zeq_0*i012;
vr_1 = vth_1 - zeq_1*i012;
vr_2 = - zeq_2*i012;
fprintf(1, 'vr_0 = ');print_comp_pol(vr_0);fprintf(1, ' [pu]\n');             
fprintf(1, 'vr_1 = ');print_comp_pol(vr_1);fprintf(1, ' [pu]\n');             
fprintf(1, 'vr_2 = ');print_comp_pol(vr_2);fprintf(1, ' [pu]\n');
i0seq = - (vr_0 / (zl0 + zseq0));
i1seq = (vth_1 - vr_1) / (zl1 + zseq1);
i2seq = - (vr_2 / (zl2 + zseq2));
fprintf(1, 'i0seq = ');print_comp_pol(i0seq);fprintf(1, ' [pu]\n');             
fprintf(1, 'i1seq = ');print_comp_pol(i1seq);fprintf(1, ' [pu]\n');             
fprintf(1, 'i2seq = ');print_comp_pol(i2seq);fprintf(1, ' [pu]\n');
fprintf(1, 'Contribuicoes de corrente vindas do sistema equivalente\n');
[iAseq, iBseq, iCseq] = CSim2Phase(i0seq, i1seq, i2seq);
fprintf(1, 'IAseq = ');print_comp_pol(iAseq*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'IBseq = ');print_comp_pol(iBseq*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'ICseq = ');print_comp_pol(iCseq*IB/1000);fprintf(1, ' [kA]\n');

%Item B
fprintf(1, '\nItem C - correntes no trafo\n');
it0 = i012 - i0seq;
it1 = i012 - i1seq;
it2 = i012 - i2seq;
fprintf(1, 'it0 = ');print_comp_pol(it0);fprintf(1, ' [pu]\n');             
fprintf(1, 'it1 = ');print_comp_pol(it1);fprintf(1, ' [pu]\n');             
fprintf(1, 'it2 = ');print_comp_pol(it2);fprintf(1, ' [pu]\n');
[iAts, iBts, iCts] = CSim2Phase(it0, it1, it2);
fprintf(1, 'Terminais do secundario\n');
fprintf(1, 'IAts = ');print_comp_pol(iAts*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'IBts = ');print_comp_pol(iBts*IB/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'ICts = ');print_comp_pol(iCts*IB/1000);fprintf(1, ' [kA]\n');
iNts = iAts + iBts + iCts;
fprintf(1, 'INts = ');print_comp_pol(iNts*IB/1000);fprintf(1, ' [kA]\n');             
it0p = 0;
it1p = it1*FasorModFaseDeg(1, -30);
it2p = it2*FasorModFaseDeg(1, +30);
[iAtp, iBtp, iCtp] = CSim2Phase(it0p, it1p, it2p);
fprintf(1, 'Terminais do primario\n');
IBg=100E6/(sqrt(3)*13800);
fprintf(1, 'IAtp = ');print_comp_pol(iAtp*IBg/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'IBtp = ');print_comp_pol(iBtp*IBg/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'ICtp = ');print_comp_pol(iCtp*IBg/1000);fprintf(1, ' [kA]\n');

fprintf(1, 'Enrolamentos do primario\n');
IAp = iAts*IB * 34.5/(sqrt(3)*13.8);
IBp = iBts*IB * 34.5/(sqrt(3)*13.8);
ICp = iCts*IB * 34.5/(sqrt(3)*13.8);
IBg=100E6/(sqrt(3)*13800);
fprintf(1, 'IAp = ');print_comp_pol(IAp/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'IBp = ');print_comp_pol(IBp/1000);fprintf(1, ' [kA]\n');             
fprintf(1, 'ICp = ');print_comp_pol(ICp/1000);fprintf(1, ' [kA]\n');


% FUNCOES UTILIZADAS ----------------------------------------------------

function Fase = FaseDeg(FaseRad)
  TFase = (FaseRad / pi) * 180.0;
  if(TFase>180)
    Fase = TFase - 360;
  elseif(TFase<-180)
    Fase = 360 + TFase;
  else
    Fase = TFase;
  end
end

%Faz printf de uma valor em module fase, com fase em graus 
function print_comp_pol (number)
  modulus = abs(number);
  phase  = FaseDeg(angle(number));
  fprintf(' %3.3f<%3.3f  ',modulus, phase);  
end

%Faz printf de uma valor em parte real e imaginaria
function print_real_imag (number)
  realpar=real(number);
  imagpar=imag(number);
  if(imagpar<0)
    fprintf(' %3.3f - j*%3.3f  ',realpar, abs(imagpar));  
  else
    fprintf(' %3.3f + j*%3.3f  ',realpar, imagpar);  
  end    
end

%Inicializa um numero complexo atrav�s da insercao de suas coordenadas polares
%em m�dulo e fase (em graus)
function FasorPolDeg = FasorModFaseDeg(modulo, fase)
  arg = (fase * pi)/180.0;
  FasorPolDeg = modulo * (cos(arg)+i*sin(arg));
end

%Faz o paralelo de dois componentes de impedancias z1 e z3, retornando o resultado
function ze1 = par(z1, z2)
  ze1 = 1/((1/z1)+(1/z2));
end

%Converter uma sequencia de tres grandezas de fase em tres grandezas em 
%componentes simetricas
function [val_0, val_1, val_2] = Phase2CSim(val_a, val_b, val_c)
  alfa = FasorModFaseDeg(1.0, 120);
  val_0 = (1/3)*(val_a +           val_b +           val_c);
  val_1 = (1/3)*(val_a +      alfa*val_b + alfa*alfa*val_c);
  val_2 = (1/3)*(val_a + alfa*alfa*val_b +      alfa*val_c);
end

%Converter uma sequencia de tres grandezas de fase em tres grandezas em 
%componentes simetricas
function [val_a, val_b, val_c] = CSim2Phase(val_0, val_1, val_2)
  alfa = FasorModFaseDeg(1.0, 120);
  val_a = (val_0 +           val_1 +           val_2);
  val_b = (val_0 + alfa*alfa*val_1 +      alfa*val_2);
  val_c = (val_0 +      alfa*val_1 + alfa*alfa*val_2);
end

