clear;
clc;
%-------------------------------------------------------------------

alfa = 1*exp(1i*(2*pi/3));
T = [1 1 1;
    1 alfa^2 alfa;
    1 alfa alfa^2];

%Dados do problema--------------"esquerda p/ direita"---------------

%Pot de base
Sb = 1e8;

%Gerador -- base ok
Vg = 220e3;

%Linha 01-02 -- base ok
z0_0102 = 1i*0.5;
z1_0102 = 1i*0.2;

%Linha 02-03 -- base ok
z0_0203 = 1i*0.8;
z1_0203 = 1i*0.3;

%Trafo abaixador em Estrela-Triangulo -- mudan�a de base (3*20)MVA -> 100MVA
V1t = 127*sqrt(3)*1e3; %fase para linha no primario
V2t = 88e3;
zt_60mva = 1i*0.09;
zt = zt_60mva*100/(3*20);

%Carga em triangulo
sc = 1i*82.6e6/100e6;
vc = 80/88;
zc = (vc*vc)/sc;

%-------------------------------------------------------------------

