%% Dados

alfa = [1; 1*exp(1i*(2*pi/3))^2; 1*exp(1i*(2*pi/3))];

Vg = 13.8e3/sqrt(3);
V = Vg*alfa;
Zeq = 1i*1.15;
Yeq = 1/Zeq;

%% Fonte de corrente
I = V/Zeq;
Ia = I(1);
Ib = I(2);
Ic = I(3);

%% Impedancias

% Impedancias de linha
Z12 = 15*(0.2779 + 1i*0.3921);
Z23 = 16*(0.2779 + 1i*0.3921);

% Impedancias da carga 1
Zcg1a = 83.91 + 1i*33.01;
Zcg1b = 117.47 + 1i*46.21;
Zcg1c = 73.42 + 1i*28.89;

% Impedancias da carga 2
% Em delta
Zcg2ab = 617.56 + 1i*231.35;
Zcg2bc = 540.36 + 1i*202.43;
Zcg2ca = 385.97 + 1i*144.59;
% Em estrela
Soma_delta = Zcg2ab + Zcg2bc + Zcg2ca;
Zcg2a = (Zcg2ab*Zcg2ca)/(Soma_delta);
Zcg2b = (Zcg2ab*Zcg2bc)/(Soma_delta);
Zcg2c = (Zcg2bc*Zcg2ca)/(Soma_delta);

% Impedancias da carga 3
Zcg3a = 165.26 + 1i*60.83;
Zcg3b = 198.32 + 1i*73.00;
Zcg3c = 123.95 + 1i*45.62;
Zn = 5.60 + 1i*2.06;

%% Admitancias
%ordem: A1, B1, C1, A2, B2, C2, N2, A3, B3, C3, N3 

% Admitancias de linha
Y12 = 1/Z12;
Y23 = 1/Z23;

% Admitancias da carga 1
Ycg1a = 1/Zcg1a;
Ycg1b = 1/Zcg1b;
Ycg1c = 1/Zcg1c;

% Admitancias da carga 2 em estrela
Ycg2a = 1/Zcg2a;
Ycg2b = 1/Zcg2b;
Ycg2c = 1/Zcg2c;

% Admitancias da carga 3
Ycg3a = 1/Zcg3a;
Ycg3b = 1/Zcg3b;
Ycg3c = 1/Zcg3c;
Yn = 1/Zn;

%            A1       ,         B1       ,        C1        ,         A2       ,        B2        ,        C2        ,         N2          ,         A3      ,        B3       ,       C3        ,                 N3       ;
G = [Yeq + Ycg1a + Y12,                 0,                 0,              -Y12,                 0,                 0,                    0,                0,                0,                0,                         0;
                     0, Yeq + Ycg1b + Y12,                 0,                 0,              -Y12,                 0,                    0,                0,                0,                0,                         0;
                     0,                 0, Yeq + Ycg1c + Y12,                 0,                 0,              -Y12,                    0,                0,                0,                0,                         0;    
                  -Y12,                 0,                 0, Y12 + Ycg2a + Y23,                 0,                 0,               -Ycg2a,             -Y23,                0,                0,                         0;
                     0,              -Y12,                 0,                 0, Y12 + Ycg2b + Y23,                 0,               -Ycg2b,                0,             -Y23,                0,                         0;
                     0,                 0,              -Y12,                 0,                 0, Y12 + Ycg2c + Y23,               -Ycg2c,                0,                0,             -Y23,                         0;
                     0,                 0,                 0,            -Ycg2a,            -Ycg2b,            -Ycg2c,Ycg2a + Ycg2b + Ycg2c,                0,                0,                0,                         0;
                     0,                 0,                 0,              -Y23,                 0,                 0,                    0,      Y23 + Ycg3a,                0,                0,                    -Ycg3a;
                     0,                 0,                 0,                 0,              -Y23,                 0,                    0,                0,      Y23 + Ycg3b,                0,                    -Ycg3b;
                     0,                 0,                 0,                 0,                 0,              -Y23,                    0,                0,                0,      Y23 + Ycg3c,                    -Ycg3c;
                     0,                 0,                 0,                 0,                 0,                 0,                    0,           -Ycg3a,           -Ycg3b,           -Ycg3c,Ycg3a + Ycg3b + Ycg3c + Yn;
     ];


b = [Ia; Ib; Ic; 0; 0; 0; 0; 0; 0; 0; 0];

e = G\b;


S1 = e(1)*conj(e(1))*conj(Ycg1a) + e(2)*conj(e(2))*conj(Ycg1b) + e(3)*conj(e(3))*conj(Ycg1c);
S2 = (e(4)-e(7))*conj(e(4)-e(7))*conj(Ycg2a) + (e(5)-e(7))*conj(e(5)-e(7))*conj(Ycg2b) + (e(6)-e(7))*conj(e(6)-e(7))*conj(Ycg2c);

S3_com_imp = (e(8)-e(11))*conj(e(8)-e(11))*conj(Ycg3a) + (e(9)-e(11))*conj(e(9)-e(11))*conj(Ycg3b) + (e(10)-e(11))*conj(e(10)-e(11))*conj(Ycg3c) + e(11)*conj(e(11))*conj(Yn);
S3 = (e(8)-e(11))*conj(e(8)-e(11))*conj(Ycg3a) + (e(9)-e(11))*conj(e(9)-e(11))*conj(Ycg3b) + (e(10)-e(11))*conj(e(10)-e(11))*conj(Ycg3c);


%trechos 1 - 2 
S12 = (e(1)-e(4))*conj(e(1)-e(4))*conj(Y12) + (e(2)-e(5))*conj(e(2)-e(5))*conj(Y12) + (e(3)-e(6))*conj(e(3)-e(6))*conj(Y12);
%trechos 2 - 3
S23 = (e(4)-e(8))*conj(e(4)-e(8))*conj(Y23) + (e(5)-e(9))*conj(e(5)-e(9))*conj(Y23) + (e(6)-e(10))*conj(e(6)-e(10))*conj(Y23);


Sfornecida = (S12 + S23 + S1 + S2 + S3_com_imp); 
Sfornecida_mod = abs(Sfornecida);

% > Respostas:
disp('Questao A:');
disp('> V1a:');
txt = strcat(num2str(abs(e(1)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(1))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V1b:');
txt = strcat(num2str(abs(e(2)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(2))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V1c:');
txt = strcat(num2str(abs(e(3)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(3))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');

disp('Questao B:');
disp('> V2a:');
txt = strcat(num2str(abs(e(4)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(4))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V2b:');
txt = strcat(num2str(abs(e(5)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(5))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V2c:');
txt = strcat(num2str(abs(e(6)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(6))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');

disp('Questao C:');
disp('> V3a:');
txt = strcat(num2str(abs(e(8)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(8))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V3b:');
txt = strcat(num2str(abs(e(9)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(9))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');
disp('> V3c:');
txt = strcat(num2str(abs(e(10)), '%5.4f'), ' V');
disp(txt);
txt = strcat(num2str(angle(e(10))*180/pi, '%5.4f'), ' graus');
disp(txt);
disp(' ');

disp('Questao D:');
disp('> P1:');
txt = strcat(num2str(real(S1)*1e-3, '%5.4f'), ' kW');
disp(txt);
disp(' ');

disp('Questao E:');
disp('> P2:');
txt = strcat(num2str(real(S2)*1e-3, '%5.4f'), ' kW');
disp(txt);
disp(' ');

disp('Questao F:');
disp('> P3:');
txt = strcat(num2str(real(S3_com_imp)*1e-3, '%5.4f'), ' kW');
disp(txt);
disp(' ');

disp('Questao G:');
disp('> P12:');
txt = strcat(num2str(real(S12)*1e-3, '%5.4f'), ' kW');
disp(txt);
disp(' ');

disp('Questao H:');
disp('> P23:');
txt = strcat(num2str(real(S23)*1e-3, '%5.4f'), ' kW');
disp(txt);
disp(' ');

disp('Questao I:');
disp('> Stotal:');
txt = strcat(num2str(abs(Sfornecida)*1e-3, '%5.4f'), ' kVA');
disp(txt);
disp(' ');