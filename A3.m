% Dados do transformador
Pn = 150000; % Potencia nominal (VA)
Vnpl = 13800; % Tensao nominal primaria de linha (V)
Vnsl = 220; % Tensao nominal secundaria de linha (V)    

N = Vnpl*sqrt(3)/Vnsl; % N = Np/Ns = relacao de espiras

% Dados de fase na fonte
Vpan = (13800/sqrt(3))*exp(1i*(-pi/6));
Vpbn = (13800/sqrt(3))*exp(1i*(-5*pi/6));
Vpcn = (13800/sqrt(3))*exp(1i*(pi/2));

% Modulo das correntes na carga
Isx = 250; % (A)

alfa_p = [1; -0.5-0.866025*1i; -0.5+0.866025*1i];
alfa_n = [1; -0.5+0.866025*1i; -0.5-0.866025*1i];
alfa_0 = [1; 1; 1];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% PARTE A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Is = Isx*alfa_p; % [Isa; Isb; Isc]

%Questao A1
Ist = Is(1) + Is(2) + Is(3); % Ist = Isa + Isb + Isc

%Questao A2
Ipf = Is/N; % Ipf = Corrente de fase no primario
Ipapb = Ipf(1);
Ipbpc = Ipf(2);
Ipcpa = Ipf(3);

%Questao A3
Soma = Ipapb + Ipbpc + Ipcpa;

%Questao A4
Ipa = Ipapb - Ipcpa;
Ipc = Ipcpa - Ipbpc;
Ipb = Ipbpc - Ipapb;

Ipl = [Ipa; Ipb; Ipc];

%Questao A5
Ipt = Ipa + Ipb + Ipc;

disp('Quest�o A1');
disp([abs(Ist), angle(Ist)*(180/pi)]);

disp('Quest�o A2');
disp([abs(Ipf), angle(Ipf)*(180/pi)]);

disp('Quest�o A3');
disp([abs(Soma), angle(Soma)*(180/pi)]);

disp('Quest�o A4');
disp([abs(Ipl), angle(Ipl)*(180/pi)]);

disp('Quest�o A5');
disp([abs(Ipt), angle(Ipt)*(180/pi)]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% PARTE B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Is = Isx*alfa_n; % [Isa; Isb; Isc]

%Questao B1
Ist = Is(1) + Is(2) + Is(3); % Ist = Isa + Isb + Isc

%Questao B2
Ipf = Is/N; % Ipf = Corrente de fase no primario
Ipapb = Ipf(1);
Ipbpc = Ipf(2);
Ipcpa = Ipf(3);

%Questao B3
Soma = Ipapb + Ipbpc + Ipcpa;

%Questao B4
Ipa = Ipapb - Ipcpa;
Ipc = Ipcpa - Ipbpc;
Ipb = Ipbpc - Ipapb;

Ipl = [Ipa; Ipb; Ipc];

%Questao B5
Ipt = Ipa + Ipb + Ipc;

disp('Quest�o B1');
disp([abs(Ist), angle(Ist)*(180/pi)]);

disp('Quest�o B2');
disp([abs(Ipf), angle(Ipf)*(180/pi)]);

disp('Quest�o B3');
disp([abs(Soma), angle(Soma)*(180/pi)]);

disp('Quest�o B4');
disp([abs(Ipl), angle(Ipl)*(180/pi)]);

disp('Quest�o B5');
disp([abs(Ipt), angle(Ipt)*(180/pi)]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% PARTE C %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Is = Isx*alfa_0; % [Isa; Isb; Isc]

%Questao C1
Ist = Is(1) + Is(2) + Is(3); % Ist = Isa + Isb + Isc

%Questao C2
Ipf = Is/N; % Ipf = Corrente de fase no primario
Ipapb = Ipf(1);
Ipbpc = Ipf(2);
Ipcpa = Ipf(3);

%Questao C3
Soma = Ipapb + Ipbpc + Ipcpa;

%Questao C4
Ipa = Ipapb - Ipcpa;
Ipc = Ipcpa - Ipbpc;
Ipb = Ipbpc - Ipapb;

Ipl = [Ipa; Ipb; Ipc];

%Questao C5
Ipt = Ipa + Ipb + Ipc;

disp('Quest�o C1');
disp([abs(Ist), angle(Ist)*(180/pi)]);

disp('Quest�o C2');
disp([abs(Ipf), angle(Ipf)*(180/pi)]);

disp('Quest�o C3');
disp([abs(Soma), angle(Soma)*(180/pi)]);

disp('Quest�o C4');
disp([abs(Ipl), angle(Ipl)*(180/pi)]);

disp('Quest�o C5');
disp([abs(Ipt), angle(Ipt)*(180/pi)]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% PARTE D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Is = Isx*(alfa_0 + alfa_p + alfa_n); % [Isa; Isb; Isc]

%Questao D1
Ist = Is(1) + Is(2) + Is(3); % Ist = Isa + Isb + Isc

%Questao D2
Ipf = Is/N; % Ipf = Corrente de fase no primario
Ipapb = Ipf(1);
Ipbpc = Ipf(2);
Ipcpa = Ipf(3);

%Questao D3
Soma = Ipapb + Ipbpc + Ipcpa;

%Questao D4
Ipa = Ipapb - Ipcpa;
Ipc = Ipcpa - Ipbpc;
Ipb = Ipbpc - Ipapb;

Ipl = [Ipa; Ipb; Ipc];

%Questao D5
Ipt = Ipa + Ipb + Ipc;

disp('Quest�o D1');
disp([abs(Ist), angle(Ist)*(180/pi)]);

disp('Quest�o D2');
disp([abs(Ipf), angle(Ipf)*(180/pi)]);

disp('Quest�o D3');
disp([abs(Soma), angle(Soma)*(180/pi)]);

disp('Quest�o D4');
disp([abs(Ipl), angle(Ipl)*(180/pi)]);

disp('Quest�o D5');
disp([abs(Ipt), angle(Ipt)*(180/pi)]);

% Potencias
% Nas cargas

Vpapb = Vpan - Vpbn;
Vpcpa = Vpcn - Vpan;
Vpbpc = Vpbn - Vpcn;

Vsan = Vpapb/N;
Vsbn = Vpbpc/N;
Vscn = Vpcpa/N;

Ssa = Vsan*conj(Is(1));
Ssb = Vsbn*conj(Is(2));
Ssc = Vscn*conj(Is(3));

Ss = [Ssa; Ssb; Ssc];
Sst = sum(Ss);

disp('Quest�o D6');
disp([real(Ss), imag(Ss)]);
disp([real(Sst), imag(Sst)]);

% No gerador

Spa = Vpan*conj(Ipa);
Spb = Vpbn*conj(Ipb);
Spc = Vpcn*conj(Ipc);

Sp = [Spa; Spb; Spc];
Spt = sum(Sp);

disp('Quest�o D7');
disp([real(Sp), imag(Sp)]);
disp([real(Spt), imag(Spt)]);





