% Inicialização
clear;

% ---------- Potencia de Base ----------
Sb = 100*10^6; %VA

% ---------- Trecho 1 (gerador ao T1 e T2) ----------

%Mudança de base impedancia gerador
Sbgantiga = 50*10^6; %VA
zgantiga = (1i)*0.05;
zg = zgantiga*Sb/Sbgantiga;

%Mudança de base Zcc T1 e T2
Sb1antiga = 30*10^6; %VA
zcct12antiga = (1i)*0.02;
zcct12 = zcct12antiga*Sb/Sb1antiga;

%Cargas T1 e T2 com Zcc's paralelos
zccI = zcct12/2;

%e' dY1, lembrar de somar +30 graus!

% ---------- Trecho 2 (secundario T1 e T2 ao primario T3 a T5) ----------
Vb2 = 132790.6*sqrt(3);%V
Zb2 = (Vb2*conj(Vb2))/Sb;

L1 = 100;      %km
ZL = (0.03 + 0.12*(1i))*L1;
zL = ZL/Zb2;

%Mudança de base Zcc T3, T4 e T5
Sb2antiga = 30*10^6; %VA
zcct35antiga = (1i)*0.03;
zcct35 = zcct35antiga*Sb/Sb2antiga;

%Cargas T3, T4 e T5 com Zcc's paralelos
zccII = zcct35/3;


% ---------- Trecho 3 ----------

%Carga
Scarga = 30*(10^6)/0.92;
sc = (Scarga/Sb)*0.92 + (Scarga/Sb)*sin(acos(0.92))*(1i);

% ---------- Tensões (chute inicial) ----------
v4 = 1;
vf = 1*exp(1i*pi/6);

% ---------- Valor inicial da corrente com base nos chutes e na carga ----------
icarga = conj(sc/v4);

% Loop principal

for i = 1:100
    
    % ---------- Atualização das tensões ----------
    
    v1 = vf - (zg)*(icarga);
    v2 = v1 - (zccI)*(icarga);
    v3 = v2 - (zL)*(icarga);
    v4 = (v3 - (zccII)*(icarga)); %v4=vcarga
    icarga = conj(sc/v4);

end


% ---------- Impressoes ----------
    
disp("icarga = ");
txt = strcat(num2str(abs(icarga), '%5.3f'), 'pu');
disp(txt);
disp(" ");
txt = strcat(num2str(angle(icarga)*180/pi , '%5.3f'), 'graus');
disp(txt);
disp(" ");

disp("v2 = ");
txt = strcat(num2str(abs(v2), '%5.3f'), 'pu');
disp(txt);
disp(" ");
txt = strcat(num2str(angle(v2)*180/pi , '%5.3f'), 'graus');
disp(txt);
disp(" ");

disp("v3 = ");
txt = strcat(num2str(abs(v3), '%5.3f'), 'pu');
disp(txt);
disp(" ");
txt = strcat(num2str(angle(v3)*180/pi, '%5.3f'), 'graus');
disp(txt);
disp(" ");

disp("v4 = ");
txt = strcat(num2str(abs(v4), '%5.3f'), 'pu');
disp(txt);
disp(" ");
txt = strcat(num2str(angle(v4)*180/pi, '%5.3f'), 'graus');
disp(txt);
disp(" ");

%voltando do pu para valores em V e A

S_pu = (v2-v3)*conj((v2-v3)/zL);
S = S_pu*Sb;

disp("c)P = ");
txt = strcat(num2str(real(S)/10^3, '%5.3f'), 'kW');
disp(txt);
disp(" ");
disp("c)Q = ");
txt = strcat(num2str(imag(S)/10^3, '%5.3f'), 'kVAr');
disp(txt);
disp(" ");

