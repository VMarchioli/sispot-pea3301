%% Inicializa��o

clear all;

% ---------- Imped�ncia da linha ----------
Zl = 0.04 + (0.20)*(1i);

% ---------- Fonte ----------
Vf = (138/sqrt(3))*1000;

% ---------- Tens�es (chute inicial) ----------
V2 = Vf;
V3 = V2;
V4 = V3;
V5 = V2;
V6 = V2;

% ---------- Pot�ncia das cargas ----------
S3 = (4 + (8/3)*(1i))*1e6;
S4 = (16/3 + (9.6/3)*(1i))*1e6;
S5 = (32/3 + (24/3)*(1i))*1e6;
S6 = (40/3 + (28/3)*(1i))*1e6;

% ---------- Valores constantes ----------
Z4 = (Vf*conj(Vf))/conj(S4);
I5 = conj((S5)/(Vf));
fase_I5 = atan2(imag(I5), real(I5)); 
mod_I5 = abs(I5);                    % C�lculo da fase de I5 em rela��o �
                                     % V5 para que quando a fase de V5 seja
                                     % atualizada seja f�cil atualizar a
                                     % corrente I5

% ---------- Atualiza��o das correntes com base nos chutes e nas caracter�sticas das cargas ----------
I3 = conj(S3/V3);
I4 = V4/Z4;
I6 = conj(S6/V6);

%% Loop principal

for i = 1:10000
   % ---------- Atualiza��o das tens�es ----------
    V2 = Vf - (20*Zl)*(I3 + I4 + I5 + I6);
    V3 = V2 - (32*Zl)*(I3 + I4);
    V4 = V3 - (16*Zl)*(I4);
    V5 = V2 - (28*Zl)*(I5);
    V6 = V2 - (48*Zl)*(I6);
    
    % ---------- C�lculo das correntes ----------
    I3 = conj(S3/V3);
    I4 = V4/Z4;
    fase_V5 = atan2(imag(V5), real(V5));
    [a, b] = pol2cart(fase_I5 + fase_V5, mod_I5);
    I5 = a + b*1i;
    I6 = conj(S6/V6);
end

%% Respostas das quest�es

% ---------- Quest�o 1 ----------
[fase_V2, modulo_V2] = cart2pol(real(V2),imag(V2));
fase_V2 = rad2deg(fase_V2);

[fase_V3, modulo_V3] = cart2pol(real(V3),imag(V3));
fase_V3 = rad2deg(fase_V3);

[fase_V4, modulo_V4] = cart2pol(real(V4),imag(V4));
fase_V4 = rad2deg(fase_V4);

[fase_V5, modulo_V5] = cart2pol(real(V5),imag(V5));
fase_V5 = rad2deg(fase_V5);

[fase_V6, modulo_V6] = cart2pol(real(V6),imag(V6));
fase_V6 = rad2deg(fase_V6);

tensoes = [modulo_V2/Vf fase_V2; modulo_V3/Vf fase_V3; modulo_V4/Vf fase_V4; modulo_V5/Vf fase_V5; modulo_V6/Vf fase_V6];

disp('Quest�o 1:');
txt = strcat(num2str(min(tensoes(:,1))*100, '%5.3f'), '%');
disp(txt);
disp(' ');

% ---------- Quest�o 2 ----------
disp('Quest�o 2:');
txt = strcat(num2str(1e-6*(real((Vf - V2)*conj(I3 + I4 + I5 + I6)) + real((V2 - V3)*conj(I3 + I4)) + real((V3 - V4)*conj(I4)) + real((V2 - V5)*conj(I5)) + real((V2 - V6)*conj(I6))), '%6.5f'), ' MW');
disp(txt);
disp(' ');

% ---------- Quest�o 3 ----------
disp('Quest�o 3:');
txt = strcat(num2str(I3 + I4 + I5 + I6), ' A');
disp(txt);
disp(' ');

% ---------- Quest�o 4 ----------
disp('Quest�o 4:');
txt = strcat(num2str(I6), ' A');
disp(txt);
disp(' ');

% ---------- Quest�o 5 ----------
disp('Quest�o 5:');
txt = strcat(num2str(1e-6*(real(V3*conj(I3)))), ' MW');
disp(txt);
disp(' ');

% ---------- Quest�o 6 ----------
disp('Quest�o 6:');
txt = strcat(num2str(1e-6*(real(V4*conj(I4)))), ' MW');
disp(txt);
disp(' ');