%% Potencia constante

Vg = 220;
Zlinha = 0.7112 + 0.0104*1i;
Q = (3000*tan(acos(0.8)));
S = 3000 + Q*1i;
Vnovo = Vg;
Vanterior = Vg;
Eps = 1e-10;

% primeira itera��o pra nao da merda com a verifica��o de erro

I = conj((S/Vnovo));
Vnovo = Vg - I*(Zlinha);
niteracoes = 1;

while (abs(Vanterior - Vnovo) > Eps)
    Vanterior = Vnovo;
    I = conj((S/Vnovo));
    Vnovo = Vg - I*(Zlinha);
    niteracoes = niteracoes + 1;
end

%tensao
x_V = real(Vnovo);
y_V = imag(Vnovo);
[fase_V, modulo_V] = cart2pol(x_V,y_V);
fase_V = radtodeg(fase_V);

% corrente
x_I = real(I);
y_I = imag(I);
[fase_I, modulo_I] = cart2pol(x_I,y_I);
fase_I = radtodeg(fase_I);


%% Corrente constante

Pnominal = 250;
Vg = 127;
Vnominal = 127;
Zlinha = 8.89*40e-3 + 0.13e-3*40*1i;
Q = Pnominal*tan(acos(0.7)); %verificar se indutiva ou capacitiva
S = Pnominal + Q*1i;
I = conj(S/Vnominal);
Eps = 1e-10;
niteracoes = 1;

fase_I = atan2(imag(I),real(I));
modulo_I = abs(I);
[a, b] = pol2cart(fase_I, modulo_I);
Ik = a + b*1i;
Vk = Vg - Ik*Zlinha;
fase_vanterior = 0;
fase_v = atan2(imag(Vk), real(Vk));


while abs(fase_v - fase_vanterior) > Eps
    fase_vanterior = fase_v;
    [a, b] = pol2cart(fase_I + fase_v, modulo_I);
    Ik = a + b*1i;
    Vk = Vg - Ik*Zlinha;
    fase_v = atan2(imag(Vk), real(Vk));
    niteracoes = niteracoes + 1;
end

%tensao
x_V = real(Vk);
y_V = imag(Vk);
[fase_V, modulo_V] = cart2pol(x_V,y_V);
fase_V = radtodeg(fase_V);

% corrente
x_I = real(Ik);
y_I = imag(Ik);
[fase_I, modulo_I] = cart2pol(x_I,y_I);
fase_I = radtodeg(fase_I);