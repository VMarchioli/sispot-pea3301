clc;
clear;

% ----- Potencia de Base --------
    Sb = 100e6;

% ---------- Trecho 1 ----------
    %fonte
    Vf = 1;
    
    Vb1 = 12000; 
    Ib1 = Sb/(sqrt(3)*Vb1);
    Zb1 = (Vb1^2)/Sb;

    %Mudanca de base do gerador
    Sb1antiga_g = 50e6;
    Zb1antiga_g = (Vb1^2)/Sb1antiga_g;
    Z1antiga_g = (1i)*(0.05);
    Z1_g = Z1antiga_g*Zb1antiga_g/Zb1;

    
    %Mudanca de base do trafo 
    Sb1antiga = 10e6;
    Zb1antiga = (Vb1^2)/Sb1antiga;
    Zcct1antiga = (1i)*(0.01); %dividido por dois (paralelo)
    Zcct1 = Zcct1antiga*Zb1antiga/Zb1;

    ZI = Zcct1 + Z1_g;

% ---------- Trecho 2 ---------- ok!
    Vb2 = 230000*exp(1i*(-pi/6));
    Ib2 = Sb/Vb2;
    Zb2 = (Vb2^2)/Sb;

    L23 = 100;      %ok!
    Z23 = (0.03 + 0.12*(1i))*L23;
    z23 = Z23/Zb2;
    
    %Mudanca de base 
    Sb2antiga = 10e6; %VA
    Zb2antiga = (Vb2^2)/Sb2antiga;
    Zcct2antiga = (1i)*(0.01);%dividido por 3(paralelo)
    Zcct2 = Zcct2antiga*Zb2antiga/Zb2;

    
    ZII = Zcct2 + z23;

% ---------- Trecho 3 ----------
    Vb3 = 13800;  %V
    Ib3 = Sb/(Vb3);
    Zb3 = (Vb3^2)/Sb;

    %Carga
    Scarga = 30e6*exp(1i*acos(0.92));
    Sc = (Scarga/Sb);

% ---------- Tensao e corrente (chute inicial) ----------
Vnom4 = 1;

I3 = conj(Sc/Vnom4);



for i = 1:10
    V2 = 1 - Z1_g*I3;
    V3 = V2 - (Zcct1)*(I3);
    V4 = V3 - (ZII)*(I3);
    I3 = conj(Sc/V3);
  
end

% Potencia

%S=(V3-V4)^2/ZII; N�O PODE CALCULAR POTENCIA EM PU